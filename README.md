361GRAD Google Maps Locations

Installation:

! Be sure you have access to gitlab via ssh or accesstoken !


1. Edit the contao composer.json and add these lines
```
"repositories": [
        {
            "type": "vcs",
            "url": "git@gitlab.com:361grad-elements/dse-locations.git"
        }
],
```

2. On the cli enter and install via composer
```
composer require 361grad-elements/dse-locations
```


3. Edit file app/AppKernel.php in contao and add to the `$bundles` array
```
new Dse\Locations\DseLocations(),
```

4. Go to the install.php and update db


5. Done
