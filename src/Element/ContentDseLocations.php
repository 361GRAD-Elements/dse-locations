<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\Locations\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\Database;

/**
 * Class ContentDseLocations
 *
 * @package Dse\Locations\Elements
 */
class ContentDseLocations extends ContentElement
{
    /**
     * Template Name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_locations';

    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        if (TL_MODE == 'BE') {
            $objTemplate = new BackendTemplate('be_wildcard');

            $objTemplate->wildcard = '### ' . $GLOBALS['TL_LANG']['MOD']['dse_locations'][0] . ' ###';

            return $objTemplate->parse();
        }

        return parent::generate();
    }

    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        $res = Database::getInstance()
            ->query('SELECT * FROM tl_dse_locations ORDER BY country');

        $this->Template->locations = $res->fetchAllAssoc();
    }
}
