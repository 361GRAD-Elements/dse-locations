<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

// CE Element
$GLOBALS['TL_CTE']['dse_elements']['dse_locations'] =
    'Dse\\Locations\\Element\\ContentDseLocations';

// BE Module
$GLOBALS['BE_MOD']['content']['dse_locations'] = [
    'tables' => ['tl_dse_locations'],
];
