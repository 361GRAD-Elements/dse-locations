<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['MOD']['dse_locations'] = ['361GRAD Locations','Locations verwalten.'];
