<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['tl_dse_locations']['new']    = ['Neue Location', 'Eine neue Location anlegen'];
$GLOBALS['TL_LANG']['tl_dse_locations']['edit']   = ['Location bearbeiten', 'Location ID %s bearbeiten'];
$GLOBALS['TL_LANG']['tl_dse_locations']['delete'] = ['Location löschen', 'Location ID %s löschen'];
$GLOBALS['TL_LANG']['tl_dse_locations']['show']   = ['Locationdetails', 'Details der Location ID %s anzeigen'];

$GLOBALS['TL_LANG']['tl_dse_locations']['locations_legend'] = 'Locations';
$GLOBALS['TL_LANG']['tl_dse_locations']['country']          = ['Land', 'Name des Landes'];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoCodeCountry']   = ['Geo Code', 'Geo Code Bezeichnung des Landes'];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoLat']           = ['Breitengrad', 'Angabe Breitengrad (00.00000)'];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoLong']          = ['Längengrad', 'Angabe Längengrad (00.00000)'];
$GLOBALS['TL_LANG']['tl_dse_locations']['title']            = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['street']           = ['Straße', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['postal']           = ['Postleitzahl', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['location']         = ['Ort', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['text']             = ['Freitext', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['phone']            = ['Telefon', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['mail']             = ['E-Mail Adresse', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['web']              = ['Website', ''];
