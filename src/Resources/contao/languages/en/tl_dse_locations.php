<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['tl_dse_locations']['new']    = ['New Location', 'Create a new Location'];
$GLOBALS['TL_LANG']['tl_dse_locations']['edit']   = ['Edit Location', 'Edit Location ID %s'];
$GLOBALS['TL_LANG']['tl_dse_locations']['delete'] = ['Delete Location', 'Delete Location ID %s'];
$GLOBALS['TL_LANG']['tl_dse_locations']['show']   = ['Locationdetails', 'Details of Location ID %s anzeigen'];

$GLOBALS['TL_LANG']['tl_dse_locations']['locations_legend'] = 'Locations';
$GLOBALS['TL_LANG']['tl_dse_locations']['country']          = ['Country', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoCodeCountry']   = ['Geo Code', 'Geo Code of Country'];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoLat']           = ['Latitude', 'Latitude (00.00000)'];
$GLOBALS['TL_LANG']['tl_dse_locations']['geoLong']          = ['Longitude', 'Longitude (00.00000)'];
$GLOBALS['TL_LANG']['tl_dse_locations']['title']            = ['Titel', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['street']           = ['Street', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['postal']           = ['Postal', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['location']         = ['Location', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['text']             = ['Custom Text', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['phone']            = ['Phone', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['mail']             = ['E-Mail', ''];
$GLOBALS['TL_LANG']['tl_dse_locations']['web']              = ['Website', ''];
