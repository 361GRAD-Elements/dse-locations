<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_DCA']['tl_dse_locations'] = [
    'config' => [
        'dataContainer'    => 'Table',
        'enableVersioning' => true,
        'sql'              => [
            'keys' => [
                'id' => 'primary'
            ]
        ],
    ],

    'list' => [
        'sorting'           => [
            'mode'        => 2,
            'fields'      => [
                'title'
            ],
            'flag'        => 3,
            'panelLayout' => 'sort,search,limit'
        ],
        'label'             => [
            'fields'      => [
                'title',
                'country',
                'postal',
                'location',
            ],
            'showColumns' => true,
        ],
        'global_operations' => [
            'all' => [
                'label'      => &$GLOBALS['TL_LANG']['MSC']['all'],
                'href'       => 'act=select',
                'class'      => 'header_edit_all',
                'attributes' => 'onclick="Backend.getScrollOffset()" accesskey="e"'
            ],
        ],
        'operations'        => [
            'edit'   => [
                'label' => &$GLOBALS['TL_LANG']['tl_dse_locations']['edit'],
                'href'  => 'act=edit',
                'icon'  => 'edit.gif'
            ],
            'delete' => [
                'label'      => &$GLOBALS['TL_LANG']['tl_dse_locations']['delete'],
                'href'       => 'act=delete',
                'icon'       => 'delete.gif',
                'attributes' => 'onclick="if(!confirm(\'' . $GLOBALS['TL_LANG']['MSC']['deleteConfirm']
                                . '\'))return false;Backend.getScrollOffset()"'
            ],
            'show'   => [
                'label'      => &$GLOBALS['TL_LANG']['tl_dse_locations']['show'],
                'href'       => 'act=show',
                'icon'       => 'show.gif',
                'attributes' => 'style="margin-right:3px;"'
            ],
        ],
    ],

    'palettes' => [
        'default' => '{locations_legend},country,geoCodeCountry,geoLat,geoLong,title,street,postal,location,text,phone,mail,web;'
    ],

    'fields' => [
        'id'             => [
            'sql' => 'int(10) unsigned NOT NULL auto_increment'
        ],
        'tstamp'         => [
            'sql' => "int(10) unsigend NOT NULL default '0'"
        ],
        'country'        => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['country'],
            'exclude'   => true,
            'sorting'   => true,
            'search'    => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class'  => 'w50',
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'geoCodeCountry' => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['geoCodeCountry'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'mandatory' => true,
                'maxlength' => 255,
                'tl_class'  => 'w50',
            ],
            'sql'       => "varchar(255) NOT NULL default 'de'"
        ],
        'geoLat'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['geoLat'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'mandatory' => true,
                'maxlength' => 11,
                'tl_class'  => 'w50',
            ],
            'sql'       => 'varchar(11) NULL'
        ],
        'geoLong'        => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['geoLong'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'mandatory' => true,
                'maxlength' => 11,
                'tl_class'  => 'w50',
            ],
            'sql'       => 'varchar(11) NULL'
        ],
        'title'          => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['title'],
            'exclude'   => true,
            'sorting'   => true,
            'search'    => true,
            'flag'      => 1,
            'inputType' => 'text',
            'eval'      => [
                'tl_class' => 'w50',
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'street'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['street'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'tl_class' => 'w50',
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'postal'         => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['postal'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'maxlength' => 16,
                'tl_class'  => 'w50',
            ],
            'sql'       => "varchar(16) NOT NULL default ''"
        ],
        'location'       => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['location'],
            'exclude'   => true,
            'search'    => true,
            'inputType' => 'text',
            'eval'      => [
                'tl_class' => 'w50',
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'text'           => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['text'],
            'exclude'   => true,
            'inputType' => 'textarea',
            'eval'      => [
                'rte'      => 'tinyMCE',
                'tl_class' => 'clr',
            ],
            'sql'       => 'text NULL'
        ],
        'phone'          => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['phone'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'tl_class' => 'w50',
                'rgxp'     => 'phone'
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'mail'           => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['mail'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'tl_class' => 'w50',
                'rgxp'     => 'email'
            ],
            'sql'       => "varchar(255) NOT NULL default ''"
        ],
        'web'            => [
            'label'     => &$GLOBALS['TL_LANG']['tl_dse_locations']['web'],
            'exclude'   => true,
            'inputType' => 'text',
            'eval'      => [
                'maxlength' => 85,
                'tl_class'  => 'w50',
                'rgxp'      => 'url'
            ],
            'sql'       => "varchar(85) NOT NULL default ''"
        ]
    ]
];
