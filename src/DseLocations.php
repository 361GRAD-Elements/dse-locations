<?php

/**
 * 361GRAD Locations
 *
 * @package   dse-locations
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2017 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\Locations;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Configures the 361GRAD Locations.
 */
class DseLocations extends Bundle
{
}
